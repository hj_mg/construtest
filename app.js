import React from 'react'
import { render } from 'react-dom'
import { hashHistory, Router, Route, Link } from 'react-router'
import auth from './utils/auth'

// Cargar Modulos
import Login from './components/Login'
import Dashboard from './components/Dashboard'
import Logout from './components/Logout'

const App = React.createClass({
  getInitialState() {
    return {
      loggedIn: auth.loggedIn()
    }
  },

  updateAuth(loggedIn) {
    this.setState({
      loggedIn: loggedIn
    })
  },

  componentWillMount() {
    auth.onChange = this.updateAuth
    auth.login()
  },

  render() {
    return (
      <div className="mastHeight">
        {this.state.loggedIn ? (
          <Dashboard/>
        ) : (
          <Login/>
        )}
        {this.props.children}
      </div>
    )
  }
})

function requireAuth(nextState, replace) {
  if (!auth.loggedIn()) {
    replace({
      pathname: '/login',
      state: { nextPathname: nextState.location.pathname }
    })
  }
}

render((
  <Router history={ hashHistory }>
    <Route path="/" component={ App }>
      <Route path="login" component={ Login } />
      <Route path="dashboard" component={ Dashboard } onEnter={ requireAuth } />
      <Route path="logout" component={ Logout } />
    </Route>
  </Router>
), document.getElementById('main'))
