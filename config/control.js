
var closeControl    = document.getElementById('windowControlClose'),
    minimizeControl = document.getElementById('windowControlMinimize'),
    maximizeControl = document.getElementById('windowControlMaximize');

var gui = require('nw.gui'),
    win = gui.Window.get();

// Mazimizar desactivado al inicial la app
win.isMaximized = false;

// Control para cerrar la aplicacion
closeControl.onclick = function (e) {
	// Termina seccion y cierra la app
	e.preventDefault();
	delete window.localStorage.token;
	win.close(true);
	process.exit(0);
};
// Controles de minimizar, el evento onclick debe tener desactivado su funcion
minimizeControl.onclick = function (e) {
	e.preventDefault();
	win.minimize();
};

win.on('minimize' , function () { 
	win.hide();
	win.setShowInTaskbar(true);
});

win.removeAllListeners('minimize');

maximizeControl.onclick = function () {
	if (win.isMaximized)
		win.unmaximize(); 
	else
		win.maximize();
};

win.on('maximize', function () {
	win.isMaximized = true;
});

win.on('unmaximize', function () {
	win.isMaximized = false;
});
