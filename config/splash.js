
"using strict";

// Contexto global de los objeto Document y Window
//global.document = window.document;
//global.navigator = window.navigator;

var gui = require('nw.gui'),
	win = gui.Window,
	splashWin = null;
gui.App.clearCache();

var package = require('./package.json');

setTimeout(function () {
	splashWin = win.open('index.html',{
		"frame" : false,
		"toolbar" : false,
		"position" : "center",
		"title" : "ConstrusText 1.0.0",
		"width" : 1024,
		"height" : 600,
		"transparent" : true
	});

	splashWin.on('loaded', function () {

		// 
		splashWin.show();
		splashWin.focus();
		splashWin.setAlwaysOnTop(true);
		splashWin.setAlwaysOnTop(false);
		// El archivo app.html es ocultado 
		win.get().hide();
	});
}, 2000);



var headle = document.querySelector('header');
var app = document.createElement('h1');
var version = document.createElement('span');
app.innerHTML = package.name;
version.innerHTML = package.version;

headle.appendChild(app);
headle.appendChild(version);


