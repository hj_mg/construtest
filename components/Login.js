import React from 'react'
import auth from '../utils/auth.js'

const Login = React.createClass({
  contextTypes: {
    router: React.PropTypes.object.isRequired
  },

  getInitialState() {
    return {
      error: false
    }
  },

  handleSubmit(event) {
    event.preventDefault()

    const email = this.refs.user.value
    const pass = this.refs.pass.value

    auth.login(email, pass, (loggedIn) => {
      if (!loggedIn)
        return this.setState({ error: true })

      if (this.props.state && this.props.state.nextPathname) {
        this.context.router.replace(this.props.state.nextPathname)
      } else {
        this.context.router.replace('/')
      }
    })
  },
// ui equal width height internally celled grid
  render() {
    return (
    	<div className="ui padded equal height grid">
    		<div className="eight wide center aligned black column">
    			<h1>ConstruTest</h1>
    		</div>
    		<div className="eight wide yellow column">
    			<form className="ui form" onSubmit={this.handleSubmit}>
    				<div className="ui stacked segment">
    					<div className="field">
    						<div className="ui left icon input">
    							<i className="user icon"></i>
    							<input ref="user" id="usernm" name="username" placeholder="Usuario" required />
    						</div>
    					</div>
    					<div className="field">
    						<div className="ui left icon input">
    							<div className="ui left icon input">
	    							<i className="lock icon"></i>
	    							<input ref="pass" id="passwrd" type="password" name="email" placeholder="password" required />
	    						</div>
    						</div>
    					</div>
    					<button className="ui fluid large teal submit button">Login</button>
    				</div>
    				{this.state.error && (
    					<div className="ui red small message">
    						<i className="star icon"></i>
    						Verifique que el usuario y contrasena son correctos.
    					</div>
    					// console.error('Error')
    				)}
    			</form>
    		</div>
    	</div>
    )
  }
})

export default Login

/*
<div className="ui vertical stripe quote segment">
    		<div className="center aligned row">
    			<div className="eight wide column green">
	    			<h1>ConstruTest</h1>
	    		</div>
	    		<div className="eight wide column black">
	    			<form className="ui form" onSubmit={this.handleSubmit}>
	    				<div className="ui stacked segment">
	    					<div className="field">
	    						<div className="ui left icon input">
	    							<i className="user icon"></i>
	    							<input ref="user" id="usernm" name="username" placeholder="Usuario" required />
	    						</div>
	    					</div>
	    					<div className="field">
	    						<div className="ui left icon input">
	    							<div className="ui left icon input">
		    							<i className="lock icon"></i>
		    							<input ref="pass" id="passwrd" type="password" name="email" placeholder="password" required />
		    						</div>
	    						</div>
	    					</div>
	    					<button className="ui fluid large teal submit button">Login</button>
	    				</div>
	    				{this.state.error && (
	    					<div className="ui red small message">
	    						<i className="star icon"></i>
	    						Verifique que el usuario y contrasena son correctos.
	    					</div>
	    					// console.error('Error')
	    				)}
	    			</form>
	    		</div>
    		</div>
    	</div>
				        */

