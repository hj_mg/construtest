import React from 'react'
import { Link } from 'react-router';
import auth from '../utils/auth'

//
// import Logout from './Logout'

const Dashboard = React.createClass({
  render() {
    const token = auth.getToken()

    return (
      <div>
        <div className="ui vertical inverted labeled icon color left visible thin sidebar menu">
          <Link className="item" to="/" activeClassName="active" onlyActiveOnIndex><i className="home icon"></i>Home</Link>
          <Link className="item" to="/about" activeClassName="active"><i className="dashboard icon"></i>About</Link>
          <Link className="item" to="/topics" activeClassName="active"><i className="block layout icon"></i>Topics</Link>
          <Link className="item" to="/calendar" activeClassName="active"><i className="calendar icon"></i>Calendar</Link>
          <Link className="item" to="/discussions" activeClassName="active"><i className="chat icon"></i>Discussions</Link>
          <div className="ui inverted menu">
            <Link className="item" to="setting" activeClassName="active"><i className="settings icon"></i>Settings</Link>
            <Link className="item" to="logout" activeClassName="active"><i className="sign out icon"></i>Logout</Link>
          </div>
        </div>
        <div className="pusher text container">
          {this.props.children || <Home/>}
        </div>
      </div>
    )
  }
})

const Home = React.createClass({
  render() {
    return (
      <div className="ui three column stackable padded middle aligned centered color grid container">
        <div className="row">
          <h2>Demo ConstruText</h2>
        </div>
        <div className="blue column">
          <p>Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula.</p>
        </div>
      </div>
    )
  }
})

const About = React.createClass({
  render() {
    return (
      <div className="ui three column stackable padded middle aligned centered color grid container">
        <div className="row">
          <h2>About</h2>
        </div>
        <div className="blue column">
          <p>Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula.</p>
        </div>
        <div className="black column">
          <p>Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula.</p>
        </div>
      </div>
    )
  }
})

const Topics = React.createClass({
  render() {
    return (
      <div className="ui three column stackable padded middle aligned centered color grid container">
        <div className="row">
          <h2>Topics</h2>
        </div>
        <div className="blue column">
          <p>Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula.</p>
        </div>
        <div className="black column">
          <p>Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula.</p>
        </div>
      </div>
    )
  }
})

const Calendar = React.createClass({
  render() {
    return (
      <div className="ui three column stackable padded middle aligned centered color grid container">
        <div className="row">
          <h2>Calendar</h2>
        </div>
        <div className="black row">
          <p>Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula.</p>
        </div>
      </div>
    )
  }
})

const Discussions = React.createClass({
  render() {
    return (
      <div className="ui three column stackable padded middle aligned centered color grid container">
        <div className="row">
          <h2>Discussions</h2>
        </div>
        <div className="blue column">
          <p>Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula.</p>
        </div>
        <div className="black column">
          <p>Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula.</p>
        </div>
      </div>
    )
  }
})

const Settings = React.createClass({
  render() {
    return (
      <div className="ui three column stackable padded middle aligned centered color grid container">
        <div className="row">
          <h2>Discussions</h2>
        </div>
        <div className="blue column">
          <p>Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula.</p>
        </div>
        <div className="black column">
          <p>Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula.</p>
        </div>
      </div>
    )
  }
})

export default Dashboard

/*
<div>
  <h1>Dashboard</h1>
  <p>You made it!</p>
  <p>{token}</p>
  <Link to="logout">Log out</Link>
</div>

*/