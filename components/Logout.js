import React from 'react'
import auth from '../utils/auth'

const Logout = React.createClass({
  componentDidMount() {
    auth.logout()
  },

  render() {
    // this.requireAuth
    return <p></p>
  }
})

export default Logout
