import crypto from 'crypto'

class Cipher {
	const algorithm = 'aes-256-gcm';
	const pass      = '3zTvzr3p67VC61jmV54rIYu1545x4TlY';

	// General un nuevo iv por cada vez que se encrypta
	const iv        = '60iP0h6vJoEa';

	encrypt (text) {
		let cipher = crypto.createCipheriv(algorithm, pass, iv);
		let encrypted = cipher.update(text, 'utf-8', 'hex');
		encrypted += cipher.final('hex');
		var tag = cipher.getAuthTag();
		return {
			content : encrypted,
			tag     : tag
		};
	};

	decrypt (encrypted) {
		var decipher = crypto.createDecipheriv(algorithm, pass, iv);
		decipher.setAuthTag(encrypted.tag);
		var dec = decipher.update(encrypted.content, 'hex', 'utf-8');
		dec += decipher.final('utf-8');
		return dec;
	}
 }

/*
** Para utilizarlo, debe ser instalaciada la clase y pasarle el string al metodo encrypt('string')
** Para recuperar el string se debe usar el metodo decrypt(encrypt('string'))
** Es reocmendado crear una variable para el encrypt
*/
//  Math.random().toString(36).replace(/[^A-Z-a-z-0-9]+/g, '').substr(0, 5);

export default Cipher
